const express = require('express'),
  bodyParser = require('body-parser'),
  pino = require('express-pino-logger')(),
  axios = require('axios'),
  axiosRetry = require('axios-retry');

const app = express();
// app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
app.use(pino);

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3002');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  next();
});

/****** This will check if gnome list stored is older than 1 day **********/
let gnomeList = null;
const _MS_PER_DAY = 1000 * 60 * 60 * 24;

// a and b are javascript Date objects
function dateDiffInDays(a, b) {
  a = new Date(a);
  b = new Date(b);
  const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
  const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
  return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}


function apiCall(req, res, find, id) {
  // Configure retries
  const numberOfRetries = 3;
  axiosRetry(axios, { retries: numberOfRetries });
  
  // If there is local data stored on server check if its older than 1 day, if its not, send local data, else fetch
  let check = true;  
  if (gnomeList) {
    const a = gnomeList.date, b = Date.now();
    if (dateDiffInDays(a, b) <= 0) {
      check = false
    }
  }

  if (check) {
    axios.get('https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json')
    .then(response => {

      if (find) {
        const result = response.data.Brastlewark.filter(gnome => gnome.id === id);
        
        if (result)
          res.send(result);
        else 
          res.status(200).send("Gnome not found");
      } else {

        gnomeList = {
          data: response.data,
          date: Date.now()
        };
        res.send(response.data);
      }
    })
    .catch(err => {      
      if (err.response && (err.response.status >= 400 && err.response.status <= 500)) {
        // client received an error response (5xx, 4xx)
        res.status(err.response.status).send(err.response.statusText);
      } else if (err.request) {
        // client never received a response, or request never left
        res.status(500).send("Something went wrong");
      } else {
        // anything else
        res.status(500).send("Something went wrong");
      }
    });
  } else {
    res.send(gnomeList.data); // send local stored data
  }
}

// Get gnomes
app.get('/gnomes', function(req, res) {  
  apiCall(req, res, false, null)
});

// Get gnomes and filter by id since there is no method for finding on the api
app.get('/gnomes/:id', function(req, res) {
  apiCall(req, res, true, parseInt(req.params.id))
});

app.listen(3001, () =>
  console.log('Express server is running on localhost:3001')
);
