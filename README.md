#### INSTALLATION

git clone https://gitlab.com/eshuerta/brastlewark.git
cd brastlewark
yarn run dev / npm run dev

### `yarn run dev / npm run dev`

Runs Node (localhost:3001) and React (localhost:3002).
Port of node can be changed on package.json file, on the `proxy` section, constants and server file.

### `yarn test`

Launches the test runner in the interactive watch mode. all test installed as dev dependencies

### `yarn build`/ `npm run build` 

Builds the app for production to the `build` folder, webpack minifies all js and css files

### ***************** NOTES ********************


### Added to package.json

"proxy": "http://localhost:3001"
"scripts": {
    ...
    "server": "node-env-run server --exec nodemon | pino-colada"  `LOGGING`
    "dev": "run-p server start" // RUNS NODE AND REACT
    ...
}

### Added pre-commit hook ESLINT checker

### ENVIROMENT
    Enviroment dev and prod setted as an enviroment variable called "NODE_ENV", locally setted on .env file on root,
    for PROD set enviroment variable on server with server data

### FOR CSS - SASS
    node-sass

### FOR IMAGE LAZY LOADING
    react-lazy-load-image-component

### FOR REDUX ASYNC FUNCTIONS
    redux-thunk

### EXAMPLE OF ASYNC FUNCTION WITH PROMISE on detail in case data is not stored on redux