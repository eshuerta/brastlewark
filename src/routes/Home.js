
import React from 'react'
import PageHeader from '../components/pageHeader/PageHeader'

const Home = ()  => {    
  const pageHeaderContent = <>
    <h2>
      "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."
    </h2>
    <h3>
        "There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain..."
    </h3>
  </>;

  return (
    <>
      <PageHeader content={pageHeaderContent} />
      <div className="container body_section">
        <p className="text">
          Los gnomos en esta ciudad no son realmente sociables porque tienen mucho trabajo por hacer. Por esta razón pueden tener más de un trabajo y pueden tener pocos o incluso ningún amigo. También aprecian su privacidad, por lo que han utilizado algunas imágenes aleatorias de Internet, no optimizadas específicamente para dispositivos móviles viejos. Por favor, escriba una aplicación para ayudar a nuestro equipo. Cras in imperdiet odio. Etiam a condimentum augue. Aenean tincidunt magna nibh, quis hendrerit purus dictum non. Ut risus nibh, iaculis efficitur vehicula in, lobortis ut ante. Curabitur auctor viverra eleifend. In est diam, iaculis a dolor non, rutrum pellentesque elit. Quisque vehicula accumsan consectetur. Sed sagittis nisl in luctus ornare.
        </p>
      </div>
    </>
  );
}

export default Home;