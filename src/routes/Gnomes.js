import React from 'react'
import PageHeader from '../components/pageHeader/PageHeader'
import GnomesList from '../components/list/Gnomes'

const Gnomes = ()  => {
  return <>
      <PageHeader content={ <h2>The Gnomes</h2>} />
      <GnomesList />
    </>
}
export default Gnomes;