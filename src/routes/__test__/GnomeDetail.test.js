import React from 'react'
import ReactDom from 'react-dom'
import Adapter from 'enzyme-adapter-react-16';
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import GnomeDetail from '../GnomeDetail' 
import { render, cleanup } from '@testing-library/react'
import { shallow, configure } from 'enzyme';
import "@testing-library/jest-dom/extend-expect";

import renderer from 'react-test-renderer'

const mockStore = configureMockStore();
const store = mockStore({});
configure({adapter: new Adapter()});
afterEach(cleanup)

const gmomes = {
    gnomeList: [{
        id: 0,
        name: "Tobus Quickwhistle",
        age: 306,
        friends: ["Cogwitz Chillwidget", "Tinadette Chillbuster"],
        hair_color: "Pink",
        height: 107.75835,
        professions: ["Metalworker", "Woodcarver", "Stonecarver", " Tinker", "Tailor", "Potter"],
        thumbnail: "http://www.publicdomainpictures.net/pictures/10000/nahled/thinking-monkey-11282237747K8xB.jpg",
        weight: 39.065952
    }],
    error: false
}

/*
const mockCallback = jest.fn(gnomeId => gmomes.gnomeList.filter(gnome => gnome.id === parseInt(gnomeId)));
test("Search should render correct amount of shows", () => {
    const match = { params: { id: '0' } }
    const component = mount(<GnomeDetail match={match}/>);
    const result = gmomes.gnomeList.filter(gnome => gnome.id === parseInt(gnomeId));
});
*/

describe("GnomeDetail Component", () => {
    it("should render without throwing an error", () => {
        expect(
            shallow(
                <Provider store={store}>
                    <GnomeDetail />
                </Provider>
            ).exists(<h2>Tobus Quickwhistle</h2>)
        ).toBe(true);
    });

    it("renders without crashing", () => {
        const div = document.createElement("div")
        ReactDom.render(<GnomeDetail />, div)
    })

    it("render title correctly", () => {
        const { getByTestId } = render(<GnomeDetail />)
        expect(getByTestId("gnome-detail-route")).toHaveTextContent("Tobus Quickwhistle");
    })

    it("matches snapshot", () => {
        const tree = renderer.create(<GnomeDetail />).toJSON()
        expect(tree).toMatchSnapshot();
    })
});

