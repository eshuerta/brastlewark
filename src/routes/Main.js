import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './Home'
import Gnomes from './Gnomes'
import About from './About'
import GnomeDetail from './GnomeDetail'

function Main() {
  return (
    <main>
      <Switch>
        <Route exact path='/' component={Home} />
        <Route exact path='/gnomes' component={Gnomes} />
        <Route path='/gnomes/:id' component={GnomeDetail} />
        <Route exact path='/about' component={About} />
      </Switch>
    </main>
  );
}

export default Main;









