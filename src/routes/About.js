import React from 'react'
import PageHeader from '../components/pageHeader/PageHeader'

const About = ()  => {  
  const pageHeaderContent = <h2>About Brastkewark</h2>;

  return <>
      <PageHeader content={pageHeaderContent} />
      <div className="container body_section">
        <p className="text">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc tincidunt non dui a bibendum. Sed ullamcorper eget erat eu suscipit. Duis scelerisque luctus lacus, imperdiet egestas dui fermentum tincidunt. Donec fermentum pulvinar leo, nec facilisis metus aliquam vitae. Nam imperdiet vehicula sem, sed tempor tellus auctor sed. Donec lacinia ligula eget sem lobortis, ac porta neque sodales. Cras tempus dolor odio, sed aliquam justo elementum ut.
        </p>
        <p>
          Cras in imperdiet odio. Etiam a condimentum augue. Aenean tincidunt magna nibh, quis hendrerit purus dictum non. Ut risus nibh, iaculis efficitur vehicula in, lobortis ut ante. Curabitur auctor viverra eleifend. In est diam, iaculis a dolor non, rutrum pellentesque elit. Quisque vehicula accumsan consectetur. Sed sagittis nisl in luctus ornare.
        </p>
        <p>
          Proin ac viverra urna, eget pharetra nisi. Proin at molestie velit, nec lobortis nunc. Nam id magna convallis, ullamcorper dolor quis, rhoncus magna. Curabitur convallis faucibus mattis. Pellentesque rhoncus sed diam at cursus. Donec enim tellus, ornare ut hendrerit vitae, vestibulum quis sapien. Pellentesque fermentum, leo in fermentum ornare, augue enim pretium libero, vitae sodales felis nisl vel justo. Integer fringilla sagittis vulputate. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nulla auctor leo id nunc mattis, ut ultrices ipsum vehicula. Cras congue aliquam aliquam. Vestibulum id nibh fermentum, gravida nisl sed, eleifend odio.
        </p>
        <p>
          Quisque sit amet fermentum tortor. Vestibulum fringilla pellentesque mi ac fermentum. Vivamus pretium bibendum purus et eleifend. Praesent vel nulla velit. Aliquam congue sapien sapien, sed imperdiet elit hendrerit non. Interdum et malesuada fames ac ante ipsum primis in faucibus. Cras commodo mi sed ex bibendum, laoreet pretium orci pulvinar. Donec dapibus congue neque, et aliquet nulla hendrerit a. In gravida urna eu fringilla pharetra. Ut ac arcu vel felis vulputate faucibus sit amet in eros. Phasellus eu hendrerit urna. Duis vehicula placerat velit sed malesuada. Fusce sed sodales risus. Vivamus volutpat ut est sagittis viverra. Duis suscipit consectetur volutpat.
        </p>
        <p>
          Vivamus in dui sed nibh vehicula vulputate et a nulla. In eleifend dapibus enim, nec finibus leo ultricies at. Cras sit amet auctor felis. Donec aliquet auctor mollis. Donec ac interdum dui. Pellentesque id blandit felis. Cras maximus commodo velit. Fusce tempus aliquam diam, dignissim dapibus dui imperdiet in. Nullam suscipit libero non velit ultricies condimentum. Mauris euismod placerat sagittis. Aenean convallis augue sit amet elementum ornare. Mauris ac aliquet leo. Morbi gravida auctor efficitur. Fusce lobortis lectus sit amet quam porta, non semper ipsum fermentum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nam eros urna, suscipit ac nisi in, accumsan consequat est.
        </p>
      </div>
    </>
}
export default About;