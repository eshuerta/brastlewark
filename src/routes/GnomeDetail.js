import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import axios from 'axios'
import PageHeader from '../components/pageHeader/PageHeader'
import GnomeDetail from '../components/detail'
import { config } from '../constants'

const GnomeDetailPage = (props)  => {  

  const [gnomeInfo, setGnomeInfo] = useState(false);
  const gnomeId = props.match.params.id // Get the param id

  // On load get the data
  useEffect(() => {
    // Check if there is data on the reducer, if there is a list, find by id
    if (props.gnomes.gnomeList && props.gnomes.gnomeList.length) { 
      const result = props.gnomes.gnomeList.filter(gnome => gnome.id === parseInt(gnomeId));
      if (result.length) 
        setGnomeInfo(result[0])
      else
        setGnomeInfo(false)
    } else { // Go to node server and find by ID
      getGnomeInfo().then(response => {response.error ? setGnomeInfo(response) : setGnomeInfo(response.data)});
    }
  }, []);

  // If data is not stored in redux go to node (EXAMPLE OF PROMISE AND ASYNC AWAIT)
  const getGnomeInfo = () => {
    return new Promise(async (resolve, reject) => {
      await axios.get(`${config.url.API_URL}/gnomes/${gnomeId}`)
        .then(res => {
            if (res && res.statusText === "OK" && res.data && !res.error && res.data.length) {
              resolve({error: false, data: res.data[0] });
            } else if (!res.error && res.data.length === 0) {
              resolve({error: true, data: "No result found"});
            } else {
              resolve({error: true, data: "Something went wrong, please try again"});
            }
        }).catch(err => {
            if (err.response) {
              // client received an error response (5xx, 4xx)            
              reject({error: true, data: err.response});
            } else if (err.request) {
              // client never received a response, or request never left
              reject({error: true, data: err.request});
            } else {
              // anything else
              reject({error: true, data: "Something went wrong, please try again"});
            }
        });
    })
  }
  return gnomeInfo && !gnomeInfo.error 
    ? <div data-testid="gnome-detail-route">
        <PageHeader content={<h2>{gnomeInfo.name}</h2>} />
        <div className="container body_section">
          <GnomeDetail gnomeInfo={gnomeInfo} />
        </div>
      </div>
    : <div data-testid="gnome-detail-route">
        <PageHeader content={<h2>Error</h2>} />
        <div className="container body_section">
          <p className="error">{gnomeInfo.data}</p>
        </div>
      </div>
}

const mapStateToProps = state => {
  return { gnomes: state.gnomes }
};

export default connect(mapStateToProps, null)(GnomeDetailPage);
