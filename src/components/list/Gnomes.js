
import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { gnomesList } from '../../actions/gnomesActions'
import GnomeListItem from './GnomeListItem'
import Paginator from './paginator'
import Filter from './filter'

const Gnomes  = ({ gnomeList, gnomes }) => {

  const [resultsPerPage, setResultsPerPage] = useState(8);
  const [dataToShow, setDataToShow] = useState([]);
  const [gnomesFiltered, setGnomesFiltered] = useState([]);
  const [offset, setOffset] = useState(0);

  // On load get the data
  useEffect(() => {
    // Check if the reducer has data in it, if not, fetch data from NODE
    if (!gnomes.gnomeList.length) 
      gnomeList();
  }, []);

  useEffect(() => {
    if (gnomes && gnomes.gnomeList && gnomes.gnomeList.length) {
      setGnomesFiltered(gnomes)
    }
    
    // shows the first results as soon as there is data on the store
    if (gnomesFiltered && gnomesFiltered.gnomeList) {
      setDataToShow(gnomesFiltered.gnomeList.slice(offset, offset + parseInt(resultsPerPage)));
      setGnomesFiltered(gnomesFiltered);
    } else 
      setDataToShow(gnomes.gnomeList.slice(0, resultsPerPage));
  }, [gnomes, resultsPerPage]);

  // Paginator function
  const handlePageClick = (data) => {    
    let selected = data.selected;
    const currentOffset = Math.ceil(selected * resultsPerPage);
    setOffset(currentOffset);
    setDataToShow(gnomesFiltered.gnomeList.slice(currentOffset, currentOffset + parseInt(resultsPerPage)));
  };

  // Filter gnomes by name, weight, height and age
  const filterGnomes = (name, weight, height, age) => {
    let lowerValue;
    if (name && name.trim() !== "")
      lowerValue = name.toLowerCase();
    if (gnomes && gnomes.gnomeList && Array.isArray(gnomes.gnomeList) && gnomes.gnomeList.length) {
      const result = gnomes.gnomeList.filter(gnome => 
        gnome.weight >= weight && gnome.height >= height && gnome.age >= age &&
        (lowerValue && lowerValue !== "" ? gnome.name.toLowerCase().includes(lowerValue) : 1 === 1)
      );
      setGnomesFiltered({ gnomeList: result, error: false });
      setDataToShow(result.slice(offset, offset + resultsPerPage))
    }
  }  

  return (
    <>
      <Filter filterGnomes={filterGnomes} />
      <div className="container body_section">
          <div className="row">
            {gnomes.error ? <p className="error">{ gnomes.gnomeList }</p> 
            : dataToShow.map( gnome => 
              <GnomeListItem gnomeInfo={gnome} key={gnome.id} />
            )}
          </div>
      </div>
      {!gnomesFiltered.error && gnomesFiltered && gnomesFiltered.gnomeList && gnomesFiltered.gnomeList.length &&
        <Paginator
          gnomes={gnomesFiltered}
          changeResultsPerPage={value => setResultsPerPage(value)}
          handlePage={value => handlePageClick(value)}
          resultsPerPage={resultsPerPage}
        />
      }
    </>
  );    
}

const mapStateToProps = state => {
  return { gnomes: state.gnomes }
};

const mapDispatchToProps = dispatch => ({
    gnomeList: () => dispatch(gnomesList())
});

Gnomes.propTypes = {
  gnomes: PropTypes.object.isRequired,
  gnomeList: PropTypes.func
};

export default connect(mapStateToProps, mapDispatchToProps)(Gnomes);

/* 
  ********************************************************************************************************
  FOR CLASS COMPONENT WITH NO NODE SERVER ANDLOCALSTORAGE TO STORE DATA TEMPORALY
  ********************************************************************************************************
  state = {
    brastlewarkList: {
      error: false,
      list: [],
    },
    // ttlStorage: 24*60*60 // 1 day
  }

  componentDidMount() {
    // Checks if localstorage is available, if "BrastlewarkList" key exists and is not empty and if its still valid by date
    if (this.storageAvailable('localStorage') && !this.checkExpired('BrastlewarkList') ) {
      this.populateStorage();
    } else {
      // If its on storage use it
      const itemStr = localStorage.getItem('BrastlewarkList');
      const item = JSON.parse(itemStr);

      // Check if data on storage its a list and have values
      if (Array.isArray(item.value) && item.value.length) {
        this.setState({
          brastlewarkList: { error: false, list: item.value }
        });
      } else {
        this.populateStorage(); // If something's wrong with the data stored, call api again and re-store data
      }  
    }
  }

  // Check if type of storage is available  (code from internet)
  storageAvailable(type) {
    try {
        var storage = window[type],
            x = '__storage_test__';
        storage.setItem(x, x);
        storage.removeItem(x);
        return true;
    }
    catch(e) {
      return e instanceof DOMException && (
        // everything except Firefox
        e.code === 22 ||
        // Firefox
        e.code === 1014 ||
        // test name field too, because code might not be present
        // everything except Firefox
        e.name === 'QuotaExceededError' ||
        // Firefox
        e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
        // acknowledge QuotaExceededError only if there's something already stored
        storage.length !== 0;
    }
  }

  // Checks if localstorage key exists and  is not expired
  checkExpired(key) {
    const itemStr = localStorage.getItem(key)
    // if the item doesn't exist, return null
    if (!itemStr) {
      return null
    }

    const item = JSON.parse(itemStr)
    const now = new Date()
    
    // compare the expiry time of the item with the current time
    if (now.getTime() > item.expiry) {
      // If the item is expired, delete the item from storage
      // and return null
      localStorage.removeItem(key)
      return null
    }
    return item.value
  }

  // get data and store on localstorage
  populateStorage = async () => {
    const result = await this.getExternalData(); // fetch data
    if (Array.isArray(result) && result.length) {      
      const now = new Date(); // Get the current datetime
      const list = {
        expiry: now.getTime() + this.state.ttlStorage, // add to the current datetime the TTL seted on state 
        value: result
      }
      localStorage.setItem('BrastlewarkList', JSON.stringify(list)); // store on localstorage for next calls
      this.setState({
        brastlewarkList: { error: false, list: result } 
      });
    } else {
      this.setState({
        brastlewarkList: { error: true, list: result }  // set error true and error message on list
      });
    }
  }

  // Get the data
  getExternalData() {
    return new Promise((resolve, reject) => {
      fetch('https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json')
        .then(response => {
          if (response.ok && response.status >= 200 && response.status < 300) {
            response.json().then(responseJson => {
              if (responseJson.Brastlewark) 
                resolve(responseJson.Brastlewark);
              else  // if key doesnt exist assume error
                resolve("Something went wrong, please try again later."); 
            });
          } else {
            reject(new Error(response.statusText))
          }
        })
        .catch(error => {
          reject(new Error(error.message))
        });
    });
  }
  */
