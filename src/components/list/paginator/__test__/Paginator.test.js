import React from 'react'
import ReactDom from 'react-dom'
import Adapter from 'enzyme-adapter-react-16';
import { configure, mount } from 'enzyme';
import Paginator from '../' 
import { render, cleanup } from '@testing-library/react'
import "@testing-library/jest-dom/extend-expect";
import renderer from 'react-test-renderer'

const mockCallback = jest.fn(value => value);
const resultsPerPage = 8
const gmomes = {
    gnomeList: [{
        id: 0,
        name: "Tobus Quickwhistle",
        age: 306,
        friends: ["Cogwitz Chillwidget", "Tinadette Chillbuster"],
        hair_color: "Pink",
        height: 107.75835,
        professions: ["Metalworker", "Woodcarver", "Stonecarver", " Tinker", "Tailor", "Potter"],
        thumbnail: "http://www.publicdomainpictures.net/pictures/10000/nahled/thinking-monkey-11282237747K8xB.jpg",
        weight: 39.065952
    }],
    error: false
}

const paginatorElement = <Paginator
    gnomes={gmomes}
    changeResultsPerPage={value => mockCallback(value)}
    handlePage={value => mockCallback(value)}
    resultsPerPage={resultsPerPage}
/>
configure({adapter: new Adapter()});

describe('Paginator', () => {

    it ('Test page results select changed', () => {
        const mockResultPerPageChanged = jest.fn();
        const wrapper = mount(
        <Paginator
            gnomes={gmomes}
            changeResultsPerPage={value => mockResultPerPageChanged(value)}
            handlePage={value => mockCallback(value)}
            resultsPerPage={resultsPerPage}
        />);
       
        wrapper.find('select').simulate('change', {target: { value : '8'}});
    });

    it("renders without crashing", () => {
        const div = document.createElement("div")
        ReactDom.render(paginatorElement, div)
    })
    
    it("render paginator correctly", () => {
        const { getByTestId } = render(paginatorElement)
        expect(getByTestId("paginator")).toHaveTextContent("Results per page");
    })
    
    it("matches snapshot", () => {
        const tree = renderer.create(paginatorElement).toJSON()
        expect(tree).toMatchSnapshot();
    })

});


afterEach(cleanup)
