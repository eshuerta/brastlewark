import React from 'react'
import PropTypes from 'prop-types'
import ReactPaginate from 'react-paginate';
import './paginator.scss'


const Paginator = ({ gnomes, changeResultsPerPage, resultsPerPage, handlePage }) => {
  
  const handlePageClick = (data) => {
    handlePage(data);
  };

  return (
    <div data-testid="paginator">
      <ReactPaginate
        previousLabel={'previous'}
        nextLabel={'next'}
        breakLabel={'...'}
        breakClassName={'break-me'}
        pageCount={gnomes ? Math.ceil(gnomes.gnomeList.length / resultsPerPage) : 0}
        marginPagesDisplayed={2}
        pageRangeDisplayed={5}
        onPageChange={(data) => handlePageClick(data)}
        containerClassName={'pagination'}
        subContainerClassName={'pages pagination'}
        activeClassName={'active'}
      />
      <div className="resultsPerPageContainer">
        <label htmlFor="resultsPerPage">Results per page</label> 
        <select
          className="form-control col-1"
          id="resultsPerPage"
          defaultValue={8}
          onChange={(e) => {
            changeResultsPerPage(e.target.value)
          }}
        >
          <option value="4">4</option>
          <option value="8">8</option>
          <option value="12">12</option>
          <option value="16">16</option>
          <option value="20">20</option>
        </select>
      </div>
    </div>
  );
}

Paginator.propTypes = {
  gnomes: PropTypes.object.isRequired,
  changeResultsPerPage: PropTypes.func.isRequired,
  resultsPerPage: PropTypes.number.isRequired,
  handlePage: PropTypes.func.isRequired
};

export default Paginator;