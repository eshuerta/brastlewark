
import React, { useState } from 'react'
import FilterItem from './FilterItem'

const Filter  = ({ filterGnomes }) => {

    const [name, setName] = useState("")
    const [weight, setWeight] = useState(1)
    const [height, setHeight] = useState(1)
    const [age, setAge] = useState(1)

    return (
    <div className="container">
        <div className="row">
            <FilterItem
                data={{ 
                    label: 'Name',
                    id: 'filterByName',
                    type: 'text',
                    placeholder: 'Eg: Tobus',
                    className: 'form-control',
                    value: name
                }}
                change={value => {
                    setName(value);
                    filterGnomes(value, weight, height, age)
                }}
            />
            <FilterItem
                data={{
                    label: 'Weight',
                    id: 'filterByWeight',
                    type: 'range',
                    className: 'custom-range',
                    min: '1',
                    max: '50',
                    value: weight
                }}
                change={value => {
                    setWeight(value);
                    filterGnomes(name, value, height, age)
                }}
            />
            <FilterItem
                data={{
                    label: 'Height',
                    id: 'filterByHeight',
                    type: 'range',
                    placeholder: '',
                    className: 'custom-range',
                    min: '1',
                    max: '130',
                    value: height
                }}
                change={value => {
                    setHeight(value);
                    filterGnomes(name, weight, value, age)
                }}
            />
            <FilterItem
                data={{
                    label: 'Age',
                    id: 'filterByAge',
                    type: 'range',
                    placeholder: '',
                    className: 'custom-range',
                    min: '1',
                    max: '400',
                    value: age
                }}
                change={value => {
                    setAge(value);
                    filterGnomes(name, weight, height, value)
                }}
            />
        </div>
    </div>);
}

export default Filter;