
import React from 'react'

const FilterItem = ({ data, change }) => {

    return <div className="form-group col-12 col-md-3" data-testid="filter_item">
                <label htmlFor={data.id}>
                    Filter by {data.label}
                    <strong style={{ color: '#ce4031', marginLeft: '20px' }}>{data.label !== 'Name' && data.value}</strong>
                </label>
                <input
                    onChange={ e => change(e.target.value)}
                    {...data}
                />
            </div>
}

export default FilterItem;