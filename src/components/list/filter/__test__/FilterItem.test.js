import React from 'react'
import ReactDom from 'react-dom'
import FilterItem from '../FilterItem' 
import { render, cleanup } from '@testing-library/react'
import "@testing-library/jest-dom/extend-expect";

import renderer from 'react-test-renderer'

const data = {
    label: 'Height',
    id: 'filterByHeight',
    type: 'range',
    placeholder: '',
    className: 'custom-range',
    min: '1',
    max: '130',
    value: 1
}

const mockCallback = jest.fn(value => value);

afterEach(cleanup)
it("renders without crashing", () => {
    const div = document.createElement("div")
    ReactDom.render(<FilterItem data={data} change={value => mockCallback(value)} />, div)
})

it("render filter correctly", () => {
    const { getByTestId } = render(<FilterItem data={data} change={value => mockCallback(value)} />)
    expect(getByTestId("filter_item")).toHaveTextContent(`Filter by ${data.label}`);
})

it("matches snapshot", () => {
    const tree = renderer.create(<FilterItem data={data} change={value => mockCallback(value)} />).toJSON()
    expect(tree).toMatchSnapshot();
})