import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import LazyImg from '../utils/LazyImg'
import './Gnomes.scss'

const GnomeListItem = ({ gnomeInfo })  => 
    <div className="flip-card col-12 col-md-3">
        <div className="flip-card-inner">
            <div className="flip-card-front">
                <h5 className="card-title">{gnomeInfo.name}</h5>
                <LazyImg
                    image={{
                        alt: gnomeInfo.name,
                        src: gnomeInfo.thumbnail
                    }}
                />
            </div>
            <div className="flip-card-back">
                <div className="card-body">                            
                    <h6 className="card-subtitle mb-2">Hair color: {gnomeInfo.hair_color}</h6>
                    <h6 className="card-subtitle mb-2">Weight: {gnomeInfo.weight}</h6>
                    <h6 className="card-subtitle mb-2">Height: {gnomeInfo.height}</h6>
                    <h6 className="card-subtitle mb-2">Age: {gnomeInfo.age}</h6>
                    <p>
                        <Link to={`/gnomes/${gnomeInfo.id}`}>CLICK FOR DETAILS</Link>
                    </p>
                </div>
            </div>
        </div>
    </div>

GnomeListItem.propTypes = {
    gnomeInfo: PropTypes.object.isRequired,
};

export default GnomeListItem;