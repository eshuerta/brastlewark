
import React from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';

const LazyImg = ({ image }) => (
  <LazyLoadImage
    data-testid="LazyImg"
    alt={image.alt}
    src={image.src}
  />
);

export default LazyImg;