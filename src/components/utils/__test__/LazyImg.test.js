import React from 'react'
import ReactDom from 'react-dom'
import LazyImg from '../LazyImg' 
import { render, cleanup } from '@testing-library/react'
import "@testing-library/jest-dom/extend-expect";

import renderer from 'react-test-renderer'

afterEach(cleanup)
it("renders without crashing", () => {
    const div = document.createElement("div")
    ReactDom.render(<LazyImg image={{ alt: "some alt text for random image", src: "https://avatars0.githubusercontent.com/u/15199?v=4" }} />, div)
})

it("render img correctly", () => {
    const { getByTestId } = render(<LazyImg image={{ alt: "some alt text for random image", src: "https://avatars0.githubusercontent.com/u/15199?v=4" }} />)
    expect(getByTestId("LazyImg")).toHaveAttribute("src")
    expect(getByTestId("LazyImg")).toHaveAttribute("alt")
})

it("matches snapshot", () => {
    const tree = renderer.create(<LazyImg image={{ alt: "some alt text for random image", src: "https://avatars0.githubusercontent.com/u/15199?v=4" }} />).toJSON()
    expect(tree).toMatchSnapshot();
})