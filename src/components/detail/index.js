import React from 'react'
import PropTypes from 'prop-types'
import './detail.scss'

const GnomeDetail = ({ gnomeInfo })  => {
return (
    <div data-testid="gnome-detail">
        <div className="row gnomeDetail">
            <div className="col-12 col-md-6">
                <img src={gnomeInfo.thumbnail} alt={gnomeInfo.name} align="left" />
            </div>
            <div className="col-12 col-md-6 borderLeft">
                <p><strong>Hair color:</strong> {gnomeInfo.hair_color}</p>
                <p><strong>Weight:</strong> {gnomeInfo.weight}</p>
                <p><strong>Height:</strong> {gnomeInfo.height}</p>
                <p><strong>Age:</strong> {gnomeInfo.age}</p>                
                <p><strong>Proffessions</strong></p>
                <ul>
                    {gnomeInfo.professions.map(
                    profession => <li key={profession}>{profession}</li>
                    )}
                </ul>
            </div>
        </div>
        {gnomeInfo.friends &&  gnomeInfo.friends.length > 0 && 
        <div className="row">
            <div className="col-12">
                <p className="friendsHeader">Friends</p>
                <ul>
                    {gnomeInfo.friends.map(
                        friend => <li key={friend}>{friend}</li>
                    )}
                </ul>
            </div>
        </div>}
    </div>)
}

GnomeDetail.propTypes = {
    gnomeInfo: PropTypes.object.isRequired,
};

export default GnomeDetail;