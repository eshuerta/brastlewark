import React from 'react'
import ReactDom from 'react-dom'
import GnomeDetail from '../' 
import { render, cleanup } from '@testing-library/react'
import "@testing-library/jest-dom/extend-expect";

import renderer from 'react-test-renderer'

const gonme = {
    id: 0,
    name: "Tobus Quickwhistle",
    age: 306,
    friends: ["Cogwitz Chillwidget", "Tinadette Chillbuster"],
    hair_color: "Pink",
    height: 107.75835,
    professions: ["Metalworker", "Woodcarver", "Stonecarver", " Tinker", "Tailor", "Potter"],
    thumbnail: "http://www.publicdomainpictures.net/pictures/10000/nahled/thinking-monkey-11282237747K8xB.jpg",
    weight: 39.065952
}
const gnomeComp =<GnomeDetail gnomeInfo={gonme} />

afterEach(cleanup)
it("renders without crashing", () => {
    const div = document.createElement("div")
    ReactDom.render(gnomeComp, div)
})

it("render detail correctly", () => {
    const { getByTestId } = render(gnomeComp)
    expect(getByTestId("gnome-detail")).toHaveTextContent("Hair color: Pink");
    expect(getByTestId("gnome-detail")).toHaveTextContent("Age: 306");
})

it("matches snapshot", () => {
    const tree = renderer.create(gnomeComp).toJSON()
    expect(tree).toMatchSnapshot();
})