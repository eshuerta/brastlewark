import React from 'react'
import Header from './Header'
import Main from '../routes/Main'
import Footer from './Footer'
import '../styles/main.scss'

const App = () => (
  <div>
    <Header />
    <Main />
    <Footer />
  </div>
)

export default App