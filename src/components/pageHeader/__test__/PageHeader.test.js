import React from 'react'
import ReactDom from 'react-dom'
import PageHeader from '../PageHeader' 
import { render, cleanup } from '@testing-library/react'
import "@testing-library/jest-dom/extend-expect";

import renderer from 'react-test-renderer'

afterEach(cleanup)
it("renders without crashing", () => {
    const div = document.createElement("div")
    ReactDom.render(<PageHeader content={<p>TEST</p>} />, div)
})

it("render header correctly", () => {
    const { getByTestId } = render(<PageHeader content={<p>TEST</p>} />)
    expect(getByTestId("header_section")).toHaveTextContent("TEST");
})

it("matches snapshot", () => {
    const tree = renderer.create(<PageHeader content={<p>TEST</p>} />).toJSON()
    expect(tree).toMatchSnapshot();
})