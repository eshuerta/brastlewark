import React from 'react'
import PropTypes from 'prop-types';

const PageHeader = ({ content })  => 

<div data-testid="header_section" className="header_section">
    <div className="container">
        {content}
    </div>
</div>

PageHeader.propTypes = {
    content: PropTypes.object.isRequired,
};

export default PageHeader;