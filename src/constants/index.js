const prod = { // sample urls
    url: {
        API_URL: 'https://brastlewark.herokuapp.com',
        API_URL_USERS: 'https://brastlewark.herokuapp.com/gnomes'
    }
};
const dev = {
    url: {
        API_URL: 'http://localhost:3001'
    },
};

export const config = process.env.NODE_ENV  === 'development' ? dev : prod;
export const FETCH_GNOMES_SUCCESS = 'FETCH_GNOMES_SUCCESS';
export const FETCH_GNOMES_ERROR = 'FETCH_GNOMES_ERROR';
