import {
    FETCH_GNOMES_SUCCESS,
    FETCH_GNOMES_ERROR,
} from '../constants/index';
  
const initialState = {
    error: false,
    gnomeList: [],
};

const gnomes = (state = initialState, action) => {
    const newState = { ...state };

    switch (action.type) {
        case FETCH_GNOMES_SUCCESS:
            newState.gnomeList = action.data;
            newState.error = false;
            return newState;
        case FETCH_GNOMES_ERROR:
            newState.gnomeList = action.data;
            newState.error = true;
            return newState;
        default:
            return state;
    }
}

export default gnomes