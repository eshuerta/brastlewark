import { combineReducers } from 'redux';
import gnomes from './gnomes'

export default combineReducers({
    gnomes,
    // .... OTHER REDUCERS....
})
