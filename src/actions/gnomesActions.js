import axios from 'axios'
import { FETCH_GNOMES_SUCCESS, FETCH_GNOMES_ERROR, config } from '../constants'

export const gnomesList = () => {
    return (dispatch) => {
        axios.get(`${config.url.API_URL}/gnomes`)
            .then(res => {
                if (res && res.statusText === "OK" && res.data && res.data.Brastlewark) {
                    dispatch({ type: FETCH_GNOMES_SUCCESS, data: res.data.Brastlewark });
                } else {
                    dispatch({ type: FETCH_GNOMES_ERROR, data: res });
                }
            }).catch(err => {
                dispatch({ type: FETCH_GNOMES_ERROR, data: err.message });
            });
    }
}

export const gnomesListError = data => ({
    type: FETCH_GNOMES_ERROR,
    data,
});
